import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { SalesListComponent } from './sales-list/sales-list.component';
import { SalesCreateComponent } from './sales-create/sales-create.component';
import { SalesUpdateComponent } from './sales-update/sales-update.component';
import { SalesViewComponent } from './sales-view/sales-view.component';
import { SalesRoutingModule } from './sales-routing.module';
import { SalesCatalogoComponent } from './sales-catalogo/sales-catalogo.component';



@NgModule({
  declarations: [SalesListComponent, SalesCreateComponent, SalesUpdateComponent, SalesViewComponent, SalesCatalogoComponent],
  imports: [
    CommonModule,
    SalesRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ]
})
export class SalesModule { }
