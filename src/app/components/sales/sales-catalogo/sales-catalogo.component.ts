import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ClientService } from '../../clients/clients.service';
import { IClient } from '../../clients/clients.model';

@Component({
  selector: 'app-sales-catalogo',
  templateUrl: './sales-catalogo.component.html',
  styleUrls: ['./sales-catalogo.component.css']
})
export class SalesCatalogoComponent implements OnInit {
  client: IClient;
  searchForm = this.fb.group({
   document: ''
  });
  constructor(
    private fb: FormBuilder,
    private clientService: ClientService
  ) { }

  ngOnInit() {
  }

  searchClient(): void{
    console.warn('DOCUMENTO....',this.searchForm.value.document);
    
    this.clientService.searchClientByDocument({
      'document-contains': this.searchForm.value.document
    })
    .subscribe(res =>{
    console.warn('RES',res);
    this.client = res[0];
    });
    
  }
}
