import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesCatalogoComponent } from './sales-catalogo.component';

describe('SalesCatalogoComponent', () => {
  let component: SalesCatalogoComponent;
  let fixture: ComponentFixture<SalesCatalogoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesCatalogoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesCatalogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
