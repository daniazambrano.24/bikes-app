import { IBike } from '../bike/bike.model';
import { IClient } from '../clients/clients.model';


export interface ISale {
    id?: number;
    dateSale: Date;
    bike: IBike;
    client: IClient;
}
export class Sale implements ISale{
    id?: number;
    dateSale: Date;
    bike: IBike;
    client: IClient;
    

}





