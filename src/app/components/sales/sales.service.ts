import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ISale } from './sales.model';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SaleService {

  constructor(private http: HttpClient) { }

  /**
   * method Query all data
   */
  public query(): Observable<ISale[]>{
    return this.http.get<ISale[]>(`${environment.END_POINT}/api/sale`)
    .pipe(map( res =>{
      return res;
    } ))
  }

  public savedSale(sale: ISale): Observable<ISale>{
    return this.http.post<ISale>(`${environment.END_POINT}/api/sale`,sale)
    .pipe(map( res =>{
      return res;
    } ))
  }
}
