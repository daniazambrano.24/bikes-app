import { Component, OnInit } from '@angular/core';
import { ISale } from '../sales.model';
import { IBike } from '../../bike/bike.model';
import { IClient } from '../../clients/clients.model';
import { BikeService } from '../../bike/bike.service';
import { ClientService } from '../../clients/clients.service';


@Component({
  selector: 'app-sales-create',
  templateUrl: './sales-create.component.html',
  styleUrls: ['./sales-create.component.css']
})
export class SalesCreateComponent implements OnInit {
  public bikeList: IBike[];
  public clientList: IClient[];
  public sale: ISale[];
 
  constructor( private bikeService: BikeService,private clientsService: ClientService ) {}
    

  ngOnInit() {
    this.bikeService.query()
    .subscribe(res =>{
      this.bikeList = res;
    });

    this.clientsService.query()
    .subscribe(res =>{
      this.clientList = res;
    });

  }

  selectedClient(id: number): void{
      console.warn('ID selected', id);
      /**this.sale. = id;*/
   
     
  }

  selectedBike(id: number): void{
    console.warn('ID selected', id);
   /**  this.sale.idBike = id;*/
    
    
  }
  save(){
    console.log(this.sale);
  }

}
