import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesListComponent } from './sales-list/sales-list.component';
import { SalesCreateComponent } from './sales-create/sales-create.component';
import { SalesUpdateComponent } from './sales-update/sales-update.component';
import { SalesViewComponent } from './sales-view/sales-view.component';
import { SalesCatalogoComponent } from './sales-catalogo/sales-catalogo.component';





const routes: Routes = [
  {
    path:'sales-catalogo',
    component:SalesCatalogoComponent
  },
  {
    path:'sales-list',
    component:SalesListComponent
    
  },
  {
    path:'sales-create',
    component:SalesCreateComponent
    
  },
  {
    path:'sales-update',
    component:SalesUpdateComponent
    
  },
  {
    path:'sales-view',
    component:SalesViewComponent
    
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesRoutingModule { }
