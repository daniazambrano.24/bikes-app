import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { IBike } from '../bike.model';
import { BikeService } from '../bike.service';

@Component({
  selector: 'app-bike-update',
  templateUrl: './bike-update.component.html',
  styleUrls: ['./bike-update.component.css']
})
export class BikeUpdateComponent implements OnInit {
  
  formBikeUpdate: FormGroup;
  constructor(private activatedRoute: ActivatedRoute,
    private bikeService: BikeService,
    private formBuilder: FormBuilder) { 
    
    this.formBikeUpdate = this.formBuilder.group({
      id: [''],
      model: ['',[Validators.required]],
      price: [''],
      serial: ['']
    });
  }
  

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    console.warn('ID', id);
    this.bikeService.getById(id)
      .subscribe(res => {
        console.warn('GET DATA', res);
        this.loadForm(res);
      }, error => { });
  }

  private loadForm(bike: IBike) {
    this.formBikeUpdate.patchValue({
      id: bike.id,
      model: bike.model,
      price: bike.price,
      serial: bike.serial

    });
  }

  update(): void {
    this.bikeService.update(this.formBikeUpdate.value)
      .subscribe(res => {
        console.warn('UPDATE OK', res)
      }, error => {
        console.warn('Error', error);
      });
  }
}
