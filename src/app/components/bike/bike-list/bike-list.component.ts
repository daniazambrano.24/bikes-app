import { Component, OnInit } from '@angular/core';
import { IBike } from '../bike.model';
import { BikeService } from '../bike.service';


@Component({
  selector: 'app-bike-list',
  templateUrl: './bike-list.component.html',
  styleUrls: ['./bike-list.component.css']
})
export class BikeListComponent implements OnInit {

  bikeList: IBike[];

  constructor(private bikeService: BikeService) { }

  ngOnInit() {
    this.bikeService.query()
    .subscribe(res =>{
      console.log('Get Data', res);
      this.bikeList = res;
    }, error =>{
      console.log("Error", error)
    })
  }

}
