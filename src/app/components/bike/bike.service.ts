import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IBike } from './bike.model';
import { map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BikeService {

  constructor(private http: HttpClient) { }

  /**
   * method Query all data
   */
  public query(): Observable<IBike[]>{
    return this.http.get<IBike[]>(`${environment.END_POINT}/api/bike`)
    .pipe(map( res =>{
      return res;
    } ))
  }

  public savedBike(bike: IBike): Observable<IBike>{
    return this.http.post<IBike>(`${environment.END_POINT}/api/bike`,bike)
    .pipe(map( res =>{
      return res;
    } ))
  }

  public getById(id: string): Observable<IBike>{
    return this.http.get<IBike>(`${environment.END_POINT}/api/bike/${id}`)
    .pipe(map( res =>{
      return res;
    } ))
  }

  public update(bike: IBike): Observable<IBike>{
    return this.http.put<IBike>(`${environment.END_POINT}/api/bike`,bike)
    .pipe(map( res =>{
      return res;
    } ))
  }
}
