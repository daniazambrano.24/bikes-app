import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';
import { BikeListComponent } from './bike-list/bike-list.component';
import { BikeUpdateComponent } from './bike-update/bike-update.component';
import { BikeCreateComponent } from './bike-create/bike-create.component';
import { BikeViewComponent } from './bike-view/bike-view.component';
import { BikeRoutingModule } from './bike-routing.module';


@NgModule({
  declarations: [BikeListComponent, BikeUpdateComponent, BikeCreateComponent, BikeViewComponent],
  imports: [
    CommonModule,
    BikeRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ]
})
export class BikeModule { }
