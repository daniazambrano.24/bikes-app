import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BikeService } from '../bike.service';

@Component({
  selector: 'app-bike-create',
  templateUrl: './bike-create.component.html',
  styleUrls: ['./bike-create.component.css']
})
export class BikeCreateComponent implements OnInit {
  bikeFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder, private bikeService: BikeService, private router:Router) {
    this.bikeFormGroup = this.formBuilder.group({
      model: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(4)
      ])],
      price: [''],
      serial: ['']
    });
  }

  ngOnInit() {
  }

  saveBike():void {
    console.warn('DATOS', this.bikeFormGroup.value);
    this.bikeService.savedBike(this.bikeFormGroup.value)
    .subscribe(res => {
        console.warn('SAVE OK ', res);
        this.router.navigate(['/bikes/bike-list']);
    }, error => {
      console.warn('Error', error);
    });
  }

}
