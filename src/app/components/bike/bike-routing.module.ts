import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BikeListComponent } from './bike-list/bike-list.component';
import { BikeCreateComponent } from './bike-create/bike-create.component';
import { BikeViewComponent } from './bike-view/bike-view.component';
import { BikeUpdateComponent } from './bike-update/bike-update.component';




const routes: Routes = [
  {
    path:'bike-list',
    component: BikeListComponent
    
  },
  {
    path:'bike-create',
    component:BikeCreateComponent
    
  },
  {
    path:'bike-update/:id',
    component:BikeUpdateComponent
    
  },
  {
    path:'bike-view',
    component:BikeViewComponent
    
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BikeRoutingModule { }
