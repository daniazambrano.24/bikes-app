import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientsCreateComponent } from './clients-create/clients-create.component';
import { ClientsUpdateComponent } from './clients-update/clients-update.component';
import { ClientsViewComponent } from './clients-view/clients-view.component';



const routes: Routes = [
  {
    path:'clients-list',
    component:ClientsListComponent
    
  },
  {
    path:'clients-create',
    component:ClientsCreateComponent
    
  },
  {
    path:'clients-update/:id',
    component:ClientsUpdateComponent
    
  },
  {
    path:'clients-view',
    component:ClientsViewComponent
    
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
