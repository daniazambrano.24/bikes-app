import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ClientService } from '../clients.service';
import { ActivatedRoute } from '@angular/router';
import { IClient } from '../clients.model';

@Component({
  selector: 'app-clients-update',
  templateUrl: './clients-update.component.html',
  styleUrls: ['./clients-update.component.css']
})
export class ClientsUpdateComponent implements OnInit {

  formClientsUpdate: FormGroup;
  constructor(private activatedRoute: ActivatedRoute,
    private clientService: ClientService,
    private formBuilder: FormBuilder) {
      this.formClientsUpdate = this.formBuilder.group({
        name: ['', Validators.compose([
          Validators.required,
          Validators.maxLength(30)
        ])],
        email: [''],
        phoneNumber: [''],
        documentNumber:['']
      });
     }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    console.warn('ID', id);
    this.clientService.getById(id)
      .subscribe(res => {
        console.warn('GET DATA', res);
        this.loadForm(res);
      }, error => { });
  }

  private loadForm(client: IClient) {
    this.formClientsUpdate.patchValue({
      id: client.id,
      name: client.name,
      email: client.email,
      phoneNumber: client.phoneNumber,
      documentNumber: client.documentNumber

    });
  }

  update(): void {
    this.clientService.update(this.formClientsUpdate.value)
      .subscribe(res => {
        console.warn('UPDATE OK', res)
      }, error => {
        console.warn('Error', error);
      });
  }

}
