import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientsUpdateComponent } from './clients-update/clients-update.component';
import { ClientsCreateComponent } from './clients-create/clients-create.component';
import { ClientsViewComponent } from './clients-view/clients-view.component';
import { ClientsRoutingModule } from './clients-routing.module';


@NgModule({
  declarations: [ClientsListComponent, ClientsUpdateComponent, ClientsCreateComponent, ClientsViewComponent],
  imports: [
    CommonModule,
    ClientsRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
    
  ]
})
export class ClientsModule { }
