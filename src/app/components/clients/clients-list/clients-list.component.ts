import { Component, OnInit } from '@angular/core';
import { IClient } from '../clients.model';
import { ClientService } from '../clients.service';


@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.css']
})
export class ClientsListComponent implements OnInit {
  clientList: IClient[];

  constructor( private clientService: ClientService) { }

  ngOnInit() {
    this.clientService.query()
    .subscribe(res =>{
      console.log('Get Data', res);
      this.clientList = res;
    }, error =>{
      console.log('Error', error);
    })
  }
  addClientToSale(client:IClient): void{

  }

}
