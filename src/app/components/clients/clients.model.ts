export interface IClient {
    id?: number;
    name: string;
    email: string;
    phoneNumber: string;
    documentNumber: String;
}

export class Client implements IClient {
    id?: number;
    name: string;
    email: string;
    phoneNumber: string;
    documentNumber: String;
    
}


