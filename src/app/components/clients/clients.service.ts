import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { IClient} from './clients.model';
import { createRequestParams } from 'src/app/utils/utils';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }


  /**
   * method Query all data
   */
  public query(): Observable<IClient[]>{
    return this.http.get<IClient[]>(`${environment.END_POINT}/api/client`)
    .pipe(map( res =>{
      return res;
    } ))
  }

  public savedClient(client: IClient): Observable<IClient>{
      return this.http.post<IClient>(`${environment.END_POINT}/api/client`,client)
    .pipe(map( res =>{
      return res;
    } ))
  }

  public getById(id: string): Observable<IClient>{
    return this.http.get<IClient>(`${environment.END_POINT}/api/client/${id}`)
    .pipe(map( res =>{
      return res;
    } ))
  }

  public update(client: IClient): Observable<IClient>{
    return this.http.put<IClient>(`${environment.END_POINT}/api/client`,client)
    .pipe(map( res =>{
      return res;
    } ))
  }
  /**
   * 
   * @param document Implements query String for request
   */
  public searchClientByDocument(req?: any): Observable<IClient[]>{
    
    let params= createRequestParams(req);
    
    return this.http.get<IClient[]>(`${environment.END_POINT}/api/client/find-by-document`,{params:params})
    .pipe(map( res =>{
      return res;
    } ));
  }

}
