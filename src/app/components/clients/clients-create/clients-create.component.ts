import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ClientService } from '../clients.service';

@Component({
  selector: 'app-clients-create',
  templateUrl: './clients-create.component.html',
  styleUrls: ['./clients-create.component.css']
})
export class ClientsCreateComponent implements OnInit {
  clientsFormGroup: FormGroup;

 
  constructor(private formBuilder: FormBuilder, private clientsService: ClientService) {
    this.clientsFormGroup = this.formBuilder.group({
      name: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(30)
      ])],
      email: [''],
      phoneNumber: [''],
      documentNumber:['']
    });
  }

  ngOnInit() {
  }

  saveClient() {
    console.log('DATOS', this.clientsFormGroup.value);
    this.clientsService.savedClient(this.clientsFormGroup.value)
    .subscribe(res => {
        console.log('SAVE OK ', res);
    }, error => {
      console.error('Error', error);
    });
  }

}
