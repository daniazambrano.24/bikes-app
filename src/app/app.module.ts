import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './auth/login/login.component';
import { AccessdeniedComponent } from './auth/accessdenied/accessdenied.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthInterceptor } from './auth/guard/auth.interceptor';
import { MainComponent } from './dashboard/main/main.component';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AccessdeniedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    {
     provide: HTTP_INTERCEPTORS,
     useClass: AuthInterceptor,
     multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
