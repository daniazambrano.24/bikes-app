import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { AccessdeniedComponent } from './auth/accessdenied/accessdenied.component';


const routes: Routes = [
  {
    path:'dashboard',
    loadChildren:() => import ('./dashboard/dashboard.module')
    .then(m =>m.DashboardModule)
  },
  {
    path:'login',
    component: LoginComponent
  },
  {
    path:'accessdenied',
    component: AccessdeniedComponent
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
