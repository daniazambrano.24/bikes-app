import { TestBed } from '@angular/core/testing';

import { UserRouteAcessGuard } from './user-route-acess.guard';

describe('UserRouteAcessGuard', () => {
  let guard: UserRouteAcessGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(UserRouteAcessGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
