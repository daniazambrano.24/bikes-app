import { Directive, OnDestroy, TemplateRef, ViewContainerRef, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { AccountService } from '../account.service';

@Directive({
  selector: '[appHasAnyAuthority]'
})
export class HasAnyAuthorityDirective implements OnDestroy {
  private authorities: string[] = [];
  private authenticationSubcriptiion?: Subscription;

  constructor(
    private accountService: AccountService,
    private templateReF: TemplateRef<any>,
    private viewContainerRef: ViewContainerRef
  ) { }

  @Input()
  set appHasAnyAuthority(value: string | string[]){
    this.authorities = typeof value === 'string' ? [value] : value;
    this.updateView();
    this.authenticationSubcriptiion = this.accountService.getAuthenticationState()
    .subscribe(() => this.updateView());

    }
    ngOnDestroy(): void{
      if(this.authenticationSubcriptiion){
        this.authenticationSubcriptiion.unsubscribe();
      }
    }
  
    

  private updateView(): void{
    const HasAnyAuthority = this.accountService.hasAnyAuthority(this.authorities);
    this.viewContainerRef.clear();
    if(HasAnyAuthority){
      this.viewContainerRef.createEmbeddedView(this.templateReF);
    }
  }

}
