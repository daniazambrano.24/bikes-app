export class Account{
    constructor(
        public activated: boolean,
        public authorities: string[],
        public email: string,
        public name: string ,
        public lastName: string, 
        public firstName: string, 
        public login: string,
    ){}
}