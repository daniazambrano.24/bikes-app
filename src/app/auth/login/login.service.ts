import { Injectable } from '@angular/core';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';
import { ICredentials } from '../models/credentials.model';
import { Account } from '../models/account.model';
import { flatMap } from 'rxjs/operators';
import { AccountService } from '../account.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private accountService: AccountService,
    private authSevice: AuthService
  ) { }

  login(credencials: ICredentials): Observable<Account | null>{
    return this.authSevice.login(credencials)
    .pipe(flatMap(() => this.accountService.identity(true)));
  }
  
  logout(): void{
    this.authSevice.logout().subscribe(null, null, () => this.accountService.authenticate(null));
  }
}