import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AuthService } from '../auth.service';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = this.fb.group({
    username: [''],
    password: [''],
    rememberMe: false

  });
  constructor(private fb: FormBuilder,
    private authService: AuthService,
    private loginService: LoginService) { }

  ngOnInit() {
  }

  login(): void {
    console.warn("Datos Login", this.loginForm.value);
    this.loginService.login(this.loginForm.value)
    .subscribe((res : any)=>{
      console.warn("ok", res);
    }, error =>{
      console.warn("error",error);
    })
  }

}
