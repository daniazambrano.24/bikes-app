import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Authority } from '../auth/constans/authorities';
import { UserRouteAcessGuard } from '../auth/guard/user-route-acess.guard';
import { MainComponent } from './main/main.component';

const routes: Routes = [
  {
    path:'',
    data:{
      authorities:[Authority.ROLE_ADMIN]
    },
    canActivate: [UserRouteAcessGuard],
    component: MainComponent,
  
  children: [
  {
    path:'bike',
    data:{
      authorities:[Authority.ROLE_ADMIN]
    },
    canActivate: [UserRouteAcessGuard],
    loadChildren:() => import('../components/bike/bike.module')
    .then(m => m.BikeModule )
  },
  {
    path:'clients',
    data:{
      authorities:[Authority.ROLE_ADMIN]
    },
    canActivate: [UserRouteAcessGuard],
    loadChildren:() => import('../components/clients/clients.module')
    .then(m => m.ClientsModule)
  },
  {
    path:'sales',
    data:{
      authorities:[Authority.ROLE_ADMIN]
    },
    canActivate: [UserRouteAcessGuard],
    loadChildren:() => import('../components/sales/sales.module')
    .then(m => m.SalesModule)
  }
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
