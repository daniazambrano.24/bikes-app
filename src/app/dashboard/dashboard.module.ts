import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BikeModule } from '../components/bike/bike.module';
import { SalesModule } from '../components/sales/sales.module';
import { ClientsModule } from '../components/clients/clients.module';
import { FooterComponent } from './footer/footer.component';
import { MainComponent } from './main/main.component';
import { AsideComponent } from './aside/aside.component';
import { AuthSharedModule } from '../auth/auth-shared/auth-shared.module';


@NgModule({
  declarations: [HomeComponent, NavbarComponent, FooterComponent, MainComponent, AsideComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    BikeModule,
    SalesModule,
    ClientsModule,
    AuthSharedModule
  ]
})
export class DashboardModule { }
